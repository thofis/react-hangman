React-Hangman
=============

Description
-----------

This is a little pet project for me to wrap my head around the react framework.
Its a implementation of the famous casual game 'Hangman'.

The project is based on this starter project: https://github.com/alicoding/react-webpack-babel

Instructions:
-------------

### Installing dependencies
npm install

### Run in dev mode
npm run dev
(server listens at http://localhost:8080)

### es-lint
npm run lint --silent
(es-lint included by following this tutorial: http://survivejs.com/webpack_react/linting_in_webpack/)
