import React from 'react';
import {Jumbotron, Alert} from 'react-bootstrap';

var HangmanActions = require('../actions/HangmanActions');

export class Display extends React.Component {
    render() {
        var messageLost = <span><strong>You lost.</strong> Press <button onClick={this.handleClick.bind(this)}>Ok</button> or hit Escape for another game? </span>,
            messageWon = <span>Congratulations! <strong>You won.</strong> Press <button onClick={this.handleClick.bind(this)}>Ok</button> or hit Escape Another game?</span>,
            messageInProcess = <span>You have <strong>{this.props.failedAttempts}</strong> failed attempts left.</span>,
            message;

        if (this.props.gameWon) {
            message = messageWon;
        } else if (this.props.gameLost) {
            message = messageLost;
        } else {
            message = messageInProcess;
        }

        return (
            <div>
                <Jumbotron>
                    <h2 className="text-center">Guess that movie</h2>
                    <pre className="text-center"><h2>{this.props.hiddenterm}</h2></pre>
                </Jumbotron>
                <Alert bsStyle="info" className="text-center">
                    {message}
                </Alert>
            </div>
        );
    }

    handleClick() {
        console.log('restart clicked');
        HangmanActions.restartGame();
    }

}