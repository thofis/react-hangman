import React from 'react';
import _ from 'lodash';
import {Grid, Row, Col} from 'react-bootstrap';

import {Header} from './header'
import {Display} from './display'
import {Picture} from './picture'
import {Keyboard} from './keyboard'

require('mousetrap'); // for handling keyboard shortcuts

var HangmanStore = require('../stores/HangmanStore');
var HangmanActions = require('../actions/HangmanActions');


// the main controller view for the game
export class Main extends React.Component {

    constructor() {
        super();
        this.initKeyboardShortcuts();
        HangmanActions.restartGame();
        this.state = HangmanStore.getGameState();
    }

    componentDidMount() {
        HangmanStore.addChangeListener(this._onChange.bind(this));
    }

    componentWillUnmount() {
        HangmanStore.removeChangeListener(this._onChange.bind(this));
    }

    _onChange() {
        this.setState(HangmanStore.getGameState());
    }

    render() {
        return (
            <Grid>
                <Row>
                    <Col md={12}>
                        <Header/>
                    </Col>
                </Row>
                <Row>
                    <Col md={3} className="hidden-xs">
                        <Picture imageNr={this.state.failedAttempts} />
                    </Col>
                    <Col md={9}>
                        <Display hiddenterm={this.state.hiddenterm}
                                 failedAttempts={this.state.limitFailedAttempts - this.state.failedAttempts}
                                 gameWon={this.state.gameWon}
                                 gameLost={this.state.gameLost}/>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <Keyboard letters={this.state.letters} />
                    </Col>
                </Row>
            </Grid>
        );
    }

    initKeyboardShortcuts() {
        var alphabet = 'abcdefghijklmnopqrstuvwxyz';
        for (var i=0; i<alphabet.length; i++) {
            let char = alphabet.charAt(i);
            Mousetrap.bind(char, ()=>{
                console.log('shortcut: '+char);
                HangmanActions.uncoverLetter(char.toUpperCase());
            });
        }
        Mousetrap.bind('esc', ()=>{ HangmanActions.restartGame(); });
    }


}