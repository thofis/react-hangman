import React from 'react';
import {Alert} from 'react-bootstrap';

export class Header extends React.Component {
    render() {
        return (
            <Alert className="text-center"><h1><u>React Hangman</u></h1></Alert>
        );
    }
}