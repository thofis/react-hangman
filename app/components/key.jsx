import React from 'react';
import {Button, ButtonGroup} from 'react-bootstrap';

var HangmanActions = require('../actions/HangmanActions');

export class Key extends React.Component {

    render() {
        var letter = this.props.letter;
        return (
            <ButtonGroup>
                <Button bsSize="large" bsStyle="default" onClick={this.handleClick.bind(this)} disabled={letter.alreadyTried}>{letter.value}</Button>
            </ButtonGroup>
        );

    }

    handleClick() {
        HangmanActions.uncoverLetter(this.props.letter.value);
    }

}