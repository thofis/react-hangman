import React from 'react';
import _ from 'lodash';
import {Button, ButtonGroup, Well} from 'react-bootstrap';
import {Key} from './key'


export class Keyboard extends React.Component {

    constructor() {
        super();
    }

    render() {
        var keys = this.props.letters.map((letter)=> {
            return (
                <Key letter={letter}/>
            );
        });
        // add 2 empty buttons for layout purpose
        keys.push(<Key letter={{value:'_',alreadyTried:true}}/>);
        keys.push(<Key letter={{value:'_',alreadyTried:true}}/>);
        var groupedKeys = _.groupBy(keys, function (element, index) {
            return Math.floor(index / 7);
        });
        return (
            <Well>
                <div>
                    <ButtonGroup justified>
                        {groupedKeys[0]}
                    </ButtonGroup>
                </div>
                <div>
                    <ButtonGroup justified>
                        {groupedKeys[1]}
                    </ButtonGroup>
                </div>
                <div>
                    <ButtonGroup  justified>
                        {groupedKeys[2]}
                    </ButtonGroup>
                </div>
                <div>
                    <ButtonGroup  justified>
                        {groupedKeys[3]}
                    </ButtonGroup>
                </div>
            </Well>
        );
    }
}