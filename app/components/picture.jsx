
import React from 'react';

export class Picture extends React.Component {
    render() {
        return (
            <img src={this.getImagePath(this.props.imageNr)} id="hangman-image" className="img-responsive img-thumbnail"></img>
        );
    }

    getImagePath(imageNr) {
        return 'images/hangman_' + imageNr + '.png';
    }
}