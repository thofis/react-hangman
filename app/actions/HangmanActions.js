var HangmanDispatcher = require('../dispatcher/dispatcher');
var HangmanConstants = require('../constants/HangmanConstants');

var HangmanActions = {

    restartGame: function () {
        HangmanDispatcher.dispatch({
            actionType: HangmanConstants.RESTART_GAME
        });
    },

    uncoverLetter: function (letter) {
        HangmanDispatcher.dispatch({
            actionType: HangmanConstants.UNCOVER_LETTER,
            letter: letter
        });
    }

};

module.exports = HangmanActions;