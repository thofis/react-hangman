import React from 'react';
import {Main} from './components/main'

export function run() {
    React.render(<Main/>, document.querySelector("#mainContainer"));
}

