var Dispatcher = require('../dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;
var HangmanConstants = require('../constants/HangmanConstants');
var assign = require('object-assign');

import {Searchterms} from './searchterms'

var CHANGE_EVENT = 'change';


var _gameState = {};

function handleKeyPressed(key) {
    console.log('Key ' + key + ' was clicked');
    //var clickedLetter = _gameState.letters.filter((letter)=>{ console.log(letter.value === key); return letter.value === key });
    var clickedLetter = _.findWhere(_gameState.letters, {'value': key});
    console.log('Letter ' + clickedLetter.value + ' was clicked');
    clickedLetter.alreadyTried = true;
    var success = applyClickedLetterOnTerm(clickedLetter);
    if (!success) {
        _gameState.failedAttempts++;
        handleGameLost();
    } else {
        handleGameWon();
    }
}

function handleRestart() {
    console.log('restart...');
    resetState();
}

function resetState() {
    var state = {
        failedAttempts: 0,
        limitFailedAttempts: 7,
        gameLost: false,
        gameWon: false
    };
    state.letters = createLetters();
    state.searchterm = getSearchterm();
    state.hiddenterm = hideCharacters(state.searchterm);
    _gameState = state;

}

function  applyClickedLetterOnTerm(letter) {
    var searchtermChars = _gameState.searchterm.split('');
    var hiddentermChars = _gameState.hiddenterm.split('');
    var successfulTry = false;
    for (var i=0; i<searchtermChars.length; i++) {
        if (searchtermChars[i].toUpperCase() === letter.value) {
            hiddentermChars[i] = searchtermChars[i];
            successfulTry = true;
        }
    }
    _gameState.hiddenterm = hiddentermChars.join('');
    return successfulTry;
}

function handleGameLost() {
    if (_gameState.failedAttempts === _gameState.limitFailedAttempts) {
        _gameState.gameLost = true;
        _gameState.hiddenterm = _gameState.searchterm;
        disableLetters();
    }

}

function handleGameWon() {
    if (_gameState.searchterm === _gameState.hiddenterm) {
        _gameState.gameWon = true;
        disableLetters();
    }

}

function handleGameWon() {
    if (_gameState.searchterm === _gameState.hiddenterm) {
        _gameState.gameWon = true;
        disableLetters();
    }

}

function hideCharacters(searchterm) {
    var alphabetRegex = /[a-zA-Z]/;
    var searchtermChars = searchterm.split('');
    var hiddentermChars = searchtermChars.map((ch) => {
        if (ch.match(alphabetRegex)) {
            return '.';
        } else {
            return ch;
        }
    });
    return hiddentermChars.join('');
}

function getSearchterm() {
    return Searchterms.movies[Math.floor(Math.random() * Searchterms.movies.length)];
}

function createLetters() {
    var alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
    var letters = alphabet.map((character)=> {
        return {value: character, alreadyTried: false};
    });
    return letters;
}

function disableLetters() {
    _gameState.letters = _gameState.letters.map((letter)=>{
        return {value:letter.value, alreadyTried: true}
    })
}

var HangmanStore = assign({}, EventEmitter.prototype, {

    getGameState: function() {
        return _gameState;
    },

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

    dispatcherIndex: Dispatcher.register(function(payload) {
        switch(payload.actionType) {
            case HangmanConstants.RESTART_GAME:
                handleRestart();
                HangmanStore.emitChange();
                break;
            case HangmanConstants.UNCOVER_LETTER:
                handleKeyPressed(payload.letter);
                HangmanStore.emitChange();
                break;
        }

        return true; // No errors. Needed by promise in Dispatcher.
    })

});

module.exports = HangmanStore;