var keyMirror = require('keymirror');

module.exports = keyMirror({
        RESTART_GAME: null,
        UNCOVER_LETTER: null
    }
);